добавить в config/app.php

в providers
        \Alexey\Basket\BasketServiceProvider::class,
в aliases
        'Basket' => \Alexey\Basket\BasketFacade::class,


все изменения сохраняються в сессии 
если надо база или редис то настраиваеться в session.php
 
	Supported: "file", "cookie", "database", "apc", "memcached", "redis", "array"
     

    'driver' => env('SESSION_DRIVER', 'file'),
		


	class TestProduct implements \Alexey\Basket\Product
	{


    private $id;
    private $price;

    /**
     * TestProduct constructor.
     * @param $id
     * @param $price
     */
    public function __construct($id, $price)
    {
        $this->id = $id;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }


}

	$product1 = new TestProduct(1, 2);
	$product2 = new TestProduct(2, 3.2);


	print_r(Basket::getProducts());
	Basket::clear();
	echo Basket::getAmount() . "\n";


	Basket::addProduct($product1);
	Basket::addProduct($product1);
	Basket::addProduct($product1);
	Basket::addProduct($product2);
	Basket::addProduct($product2);
	Basket::addProduct($product2);
	Basket::addProduct($product2);
	print_r(Basket::getProducts());
	print_r(Basket::getAmount());

	Basket::removeProduct($product1);
	Basket::removeProduct($product2);
	print_r(Basket::getProducts());
	print_r(Basket::getAmount());


	Basket::deleteProduct($product2);
	print_r(Basket::getProducts());
	echo Basket::getAmount() . "\n";


	Basket::qntProduct($product2, 10);
	print_r(Basket::getProducts());
	echo Basket::getAmount() . "\n";


	Basket::setCoupon(new \Alexey\Basket\FixDiscount(10));
	echo Basket::getAmount() . "\n";


	Basket::setCoupon(new \Alexey\Basket\PercentDiscount(10));
	echo Basket::getAmount() . "\n";


	Basket::deleteCoupon();
	echo Basket::getAmount() . "\n";


 
