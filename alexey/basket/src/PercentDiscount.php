<?php

namespace Alexey\Basket;
class PercentDiscount implements Discount
{
    private $discount;

    /**
     * PercentDiscount constructor.
     * @param $discount
     */
    public function __construct($discount)
    {
        $this->discount = $discount;
    }

    public function getAmount(Basket $basket)
    {
        $sum = 0;
        foreach ($basket->getProducts() as $product) {
            $sum += $product['price'] * $product['qnt'];
        }
        return $sum - $sum * $this->discount / 100;
    }
}