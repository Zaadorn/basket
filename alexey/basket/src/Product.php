<?php

namespace Alexey\Basket;
interface Product
{
    public function getId();

    public function getPrice();
}