<?php

namespace Alexey\Basket;

use Illuminate\Support\Facades\Facade;

class BasketFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Basket';
    }
}
