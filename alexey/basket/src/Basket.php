<?php

namespace Alexey\Basket;
class Basket
{
    private $products = [];
    private $coupon;

    /**
     * Basket constructor.
     */
    public function __construct()
    {
        $this->restore();
    }


    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }


    /**
     * @return mixed
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param mixed $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @param mixed $coupon
     */
    public function deleteCoupon()
    {
        unset($this->coupon);
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {

        if (!isset($this->products[$product->getId()])) {
            $this->products[$product->getId()] = [
                'qnt' => 1,
                'price' => $product->getPrice()
            ];
        } else {
            $this->products[$product->getId()]['qnt']++;
        }
        $this->store();
    }

    /**
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        if (isset($this->products[$product->getId()])) {
            if ($this->products[$product->getId()]['qnt'] == 1) {
                unset($this->products[$product->getId()]);
            } else {
                $this->products[$product->getId()]['qnt']--;
            }
        }
        $this->store();
    }

    /**
     * @param Product $product
     */
    public function deleteProduct(Product $product)
    {
        if (isset($this->products[$product->getId()])) {
            unset($this->products[$product->getId()]);
        }
        $this->store();
    }


    /**
     * @param Product $product
     * @param $qnt
     * @throws \ArithmeticError
     */
    public function qntProduct(Product $product, $qnt)
    {

        if ($qnt <= 0) {
            throw new \ArithmeticError('The quantity is less than 0');
        }
        if (isset($this->products[$product->getId()])) {
            $this->products[$product->getId()]['qnt'] = $qnt;
        } else {
            $this->products[$product->getId()] = [
                'qnt' => $qnt,
                'price' => $product->getPrice()
            ];
        }
        $this->store();
    }


    public function clear()
    {
        $this->products = [];
        $this->store();
    }

    public function getAmount()
    {
        if (isset($this->coupon)) {
            return $this->coupon->getAmount($this);
        } else {
            $sum = 0;
            foreach ($this->getProducts() as $product) {
                $sum += $product['price'] * $product['qnt'];
            }
            return $sum;
        }

    }

    private function store()
    {
        session(['basket' => $this->products]);
    }

    private function restore()
    {
        $this->products = session('basket');
    }

}