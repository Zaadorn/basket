<?php

namespace Alexey\Basket;
interface Discount
{

    public function getAmount(Basket $basket);
}